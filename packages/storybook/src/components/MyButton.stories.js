import { action } from "@storybook/addon-actions";
import { withKnobs, object } from "@storybook/addon-knobs";
import MyButton from "./MyButton.vue";

export default {
  title: "MyButton",
  decorators: [withKnobs],
  excludeStories: /.*Data$/,
};

export const actionsData = {
  onClickAction: action("onClickAction"),
  onEnterAction: action("onEnterAction"),
};

export const myButtonData = {
  title: "Click me",
  class: "myButton",
  type: "default",
};

export const myButtonLargeData = {
  title: "Click me large ",
  class: "myButton myButton--large",
  type: "large",
};

const myButtonTemplate = ` <MyButton :myButtonData="myButtonData" @clickAction="onClickAction" @enterAction="onEnterAction" />`;

export const Default = () => ({
  components: { MyButton },
  template: myButtonTemplate,
  props: {
    myButtonData: {
      default: () => object("myButtonData", { ...myButtonData }),
    },
  },
  methods: actionsData,
});

export const Large = () => ({
  components: { MyButton },
  template: myButtonTemplate,
  props: {
    myButtonData: {
      default: () => myButtonLargeData,
    },
  },
  methods: actionsData,
});
