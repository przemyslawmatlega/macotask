import { action } from "@storybook/addon-actions";
import MyUglyMenu from "./MyUglyMenu.vue";
import { withKnobs, object } from "@storybook/addon-knobs";

import {
  actionsData,
  myButtonData,
  myButtonLargeData,
} from "./MyButton.stories";

export default {
  title: "MyUglyMenu",
  decorators: [withKnobs],
  excludeStories: /.*Data$/,
};

export const defaultMenuData = (buttonData) =>
  new Array(5).fill().map((element, index) => ({
    ...buttonData,
    title: `${buttonData.title} ${index}`,
  }));

const menuTemplate = `<MyUglyMenu :menuData="menuData" @clickAction="onClickAction" @enterAction="onEnterAction" />`;

export const Default = () => ({
  components: { MyUglyMenu },
  template: menuTemplate,
  props: {
    menuData: {
      default: () => object("defaultMenuData", defaultMenuData(myButtonData)),
    },
  },
  methods: actionsData,
});

export const LargeMenu = () => ({
  components: { MyUglyMenu },
  template: menuTemplate,
  props: {
    menuData: {
      default: () =>
        object("defaultMenuData", defaultMenuData(myButtonLargeData)),
    },
  },
  methods: actionsData,
});
